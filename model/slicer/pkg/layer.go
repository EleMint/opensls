package pkg

import (
	"fmt"
	"math"
	"opensls/model/encoding"
	"sort"
	"sync"
)

type Layers struct {
	mu     sync.Mutex
	Layers []Layer
}

func (ls *Layers) Insert(l Layer) {
	ls.mu.Lock()
	defer ls.mu.Unlock()

	i := sort.Search(len(ls.Layers), func(i int) bool {
		return l.Z < ls.Layers[i].Z
	})

	if i < len(ls.Layers) && ls.Layers[i].Z == l.Z {
		// slice is present in data[i]
	} else {
		// sljice is not present data,
		// but i is the index where it would be inserted.
		ls.Layers = append(ls.Layers, Layer{})
		copy(ls.Layers[i+1:], ls.Layers[i:])
		ls.Layers[i] = l
	}
}

type Path struct {
	A  encoding.Vertex
	B  encoding.Vertex
	On bool
}

type Layer struct {
	Z     float32
	Paths []Path
}

type ResultCase uint

const (
	PARALLEL ResultCase = iota
	SUCCESS
	FAIL
)

type Result struct {
	Case    ResultCase
	Segment *Segment
	Point   encoding.Vertex
}

type Ray struct {
	pos encoding.Vertex
	dir encoding.Vertex
}

func NewRay(pos encoding.Vertex) Ray {
	angle := float64(270)
	dir := encoding.Vertex{X: float32(math.Cos(angle)), Y: float32(math.Sin(angle))}

	return Ray{
		pos: pos,
		dir: dir,
	}
}

func RaySegInt(ray Ray, seg *Segment) Result {
	x1 := float64(seg.Points[0].X)
	y1 := float64(seg.Points[0].Y)
	x2 := float64(seg.Points[1].X)
	y2 := float64(seg.Points[1].Y)
	x3 := float64(ray.pos.X)
	y3 := float64(ray.pos.Y)
	x4 := float64(ray.pos.X + ray.dir.X)
	y4 := float64(ray.pos.Y + ray.dir.Y)

	den := (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4)
	den = math.Round(den*1000) / 1000

	if den == 0 {
		return Result{
			Case:    PARALLEL,
			Segment: seg,
		}
	}

	t := (x1-x3)*(y3-y4) - (y1-y3)*(x3-x4)
	u := -((x1-x2)*(y1-y3) - (y1-y2)*(x1-x3))

	if !(t > 0 && t < 1 && u > 0) {
		return Result{
			Case:    FAIL,
			Segment: seg,
		}
	}

	x := x1 + t*(x2-x1)
	y := y1 + t*(y2-y1)

	return Result{
		Case:    SUCCESS,
		Segment: seg,
		Point:   encoding.Vertex{X: float32(x), Y: float32(y), Z: seg.Points[0].Z},
	}
}

func CalculateLayer(slice Slice) Layer {
	fmt.Printf("step: %v %v\n", slice.Step, slice.Z)

	minX := slice.Segments.MinX()
	minY := slice.Segments.MinY()
	maxX := slice.Segments.MaxX()
	// maxY := slice.Segments.MaxY()

	pathSize := float32(0.05)
	pathCount := int((maxX - minX) / pathSize)
	var ray Ray

	for step := 0; step <= pathCount; step++ {
		x := minX + (pathSize * float32(step))
		x = float32(math.Round(float64(x)*1000) / 1000)

		pos := encoding.Vertex{X: x, Y: minY, Z: slice.Z}
		ray = NewRay(pos)

		hits := []Result{}

		for _, seg := range slice.Segments {
			res := RaySegInt(ray, &seg)
			if res.Case == SUCCESS {
				hits = append(hits, res)
			}
		}

		fmt.Println("hit count: ", len(hits))
	}

	return Layer{
		Z:     slice.Z,
		Paths: []Path{},
	}
}
