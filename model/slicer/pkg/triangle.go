package pkg

import (
	"bytes"
	"fmt"
	"strings"

	"opensls/model/encoding"
)

// type Triangle [3]*encoding.Vertex
type Triangle struct {
	norm  encoding.Normal
	verts [3]encoding.Vertex
}

func NewTriangle(norm encoding.Normal, a, b, c encoding.Vertex) *Triangle {
	return &Triangle{
		norm:  norm,
		verts: [3]encoding.Vertex{a, b, c},
	}
}

func (t Triangle) Norm() *encoding.Normal { return &t.norm }

func (t Triangle) A() *encoding.Vertex { return &t.verts[0] }
func (t Triangle) B() *encoding.Vertex { return &t.verts[1] }
func (t Triangle) C() *encoding.Vertex { return &t.verts[2] }

func (t Triangle) Value() []byte {
	buf := bytes.Buffer{}

	a := t.verts[0]
	b := t.verts[1]
	c := t.verts[2]

	buf.Write(a.Value())
	buf.Write(b.Value())
	buf.Write(c.Value())

	return buf.Bytes()
}

func (t Triangle) String() string {
	buf := strings.Builder{}

	a := t.verts[0]
	b := t.verts[1]
	c := t.verts[2]

	buf.WriteString("{Triangle ")

	buf.WriteString(fmt.Sprintf(
		"A: %s, B: %s, C: %s",
		a, b, c,
	))

	buf.WriteString("}")

	return buf.String()
}

func IsActive(c TriangleCase) bool {
	return c == CASE_I ||
		c == CASE_II ||
		c == CASE_III ||
		c == CASE_VI ||
		c == CASE_VII // ||
	// c == CASE_X
}

func (t Triangle) MinZ() float32 {
	a := t.verts[0].Z
	b := t.verts[1].Z
	c := t.verts[2].Z

	if a <= b && a <= c {
		return a
	} else if b <= a && b <= c {
		return b
	}

	return c
}

func (t Triangle) MaxZ() float32 {
	a := t.verts[0].Z
	b := t.verts[1].Z
	c := t.verts[2].Z

	if a >= b && a >= c {
		return a
	} else if b >= a && b >= c {
		return b
	}

	return c
}
