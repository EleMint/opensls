package pkg

import (
	"fmt"
	"math"
	"opensls/model/encoding"
	"os"
	"sort"
)

type Segment struct {
	Case   TriangleCase
	Points [2]encoding.Vertex
	Normal *encoding.Normal
}

func (s Segment) MinX() float32 {
	if s.Points[0].X < s.Points[1].X {
		return s.Points[0].X
	}

	return s.Points[1].X
}

type Segments []Segment

func (ss Segments) Insert(s Segment) Segments {
	i := sort.Search(len(ss), func(i int) bool {
		return s.MinX() < ss[i].MinX()
	})

	if i < len(ss) && ss[i] == s {
		// tri is present in data[i]
	} else {
		// tri is not present in data,
		// but i is the index where it would be inserted.
		ss = append(ss, Segment{})
		copy(ss[i+1:], ss[i:])
		ss[i] = s
	}

	return ss
}

var NilSegment = Segment{}

func (ss Segments) MinX() (min float32) {
	min = math.MaxFloat32

	for _, seg := range ss {
		a := seg.Points[0].X
		b := seg.Points[1].X
		m := math.Min(float64(a), float64(b))
		min = float32(m)
	}

	return
}

func (ss Segments) MinY() (min float32) {
	min = math.MaxFloat32

	for _, seg := range ss {
		a := seg.Points[0].Y
		b := seg.Points[1].Y
		m := math.Min(float64(a), float64(b))
		min = float32(m)
	}

	return
}

func (ss Segments) MaxX() (max float32) {
	max = math.SmallestNonzeroFloat32

	for _, seg := range ss {
		a := seg.Points[0].X
		b := seg.Points[1].X
		m := math.Max(float64(a), float64(b))
		max = float32(m)
	}

	return
}

func (ss Segments) MaxY() (max float32) {
	max = math.SmallestNonzeroFloat32

	for _, seg := range ss {
		a := seg.Points[0].Y
		b := seg.Points[1].Y
		m := math.Max(float64(a), float64(b))
		max = float32(m)
	}

	return
}

func (t Triangle) Segment(c TriangleCase, pts CasePoints, z float32) Segment {
	var seg [2]encoding.Vertex

	switch c {
	case CASE_I:
		seg = t.caseIPoints(pts, z)
	case CASE_II:
		seg = t.caseIIPoints(pts, z)
	case CASE_III:
		seg = t.caseIIIPoints(pts, z)
	case CASE_VI:
		seg = t.caseVIPoints(pts, z)
	case CASE_VII:
		seg = t.caseVIIPoints(pts, z)
	default:
		fmt.Fprintln(os.Stderr, c)
		return NilSegment
	}

	return Segment{
		Case:   c,
		Points: seg,
		Normal: t.Norm(),
	}
}

func solveT(z float32, a, b encoding.Vertex) (float32, bool) {
	t := (z - a.Z) / (b.Z - a.Z)
	rev := false
	if !(t >= 0 && t <= 1) {
		t = (z - b.Z) / (a.Z - b.Z)
		rev = true
	}
	if !(t >= 0 && t <= 1) {
		fmt.Fprintf(os.Stderr, "WARNING: SolveT Z: %v A: %s B: %s\n", z, a, b)
	}

	return t, rev
}

func solve(t float32, v1, v2 encoding.Vertex, rev bool) (v encoding.Vertex) {
	if rev {
		v.X = v2.X + (t * (v1.X - v2.X))
		v.Y = v2.Y + (t * (v1.Y - v2.Y))
		v.Z = v2.Z + (t * (v1.Z - v2.Z))
	} else {
		v.X = v1.X + (t * (v2.X - v1.X))
		v.Y = v1.Y + (t * (v2.Y - v1.Y))
		v.Z = v1.Z + (t * (v2.Z - v1.Z))
	}

	return
}

func (tri Triangle) caseIPoints(pts CasePoints, z float32) [2]encoding.Vertex {
	a := pts.Above[0]
	b := pts.Below[0]
	c := pts.Below[1]

	t1, rev1 := solveT(z, a, b)
	v1 := solve(t1, a, b, rev1)

	t2, rev2 := solveT(z, a, c)
	v2 := solve(t2, a, c, rev2)

	return [2]encoding.Vertex{v1, v2}
}

func (tri Triangle) caseIIPoints(pts CasePoints, z float32) [2]encoding.Vertex {
	a := pts.Below[0]
	b := pts.Above[0]
	c := pts.Above[1]

	t1, rev1 := solveT(z, a, b)
	v1 := solve(t1, a, b, rev1)

	t2, rev2 := solveT(z, a, c)
	v2 := solve(t2, a, c, rev2)

	return [2]encoding.Vertex{v1, v2}
}

func (tri Triangle) caseIIIPoints(pts CasePoints, z float32) [2]encoding.Vertex {
	a := pts.At[0]
	b := pts.Above[0]
	c := pts.Below[0]

	t, rev := solveT(z, b, c)
	v := solve(t, b, c, rev)

	return [2]encoding.Vertex{a, v}
}

func (tri Triangle) caseVIPoints(pts CasePoints, z float32) [2]encoding.Vertex {
	a := pts.At[0]
	b := pts.At[1]

	return [2]encoding.Vertex{a, b}
}

func (tri Triangle) caseVIIPoints(pts CasePoints, z float32) [2]encoding.Vertex {
	a := pts.At[0]
	b := pts.At[1]

	return [2]encoding.Vertex{a, b}
}

func (tri Triangle) CaseXPoints() [3][2]encoding.Vertex {
	segs := [3][2]encoding.Vertex{}

	a := tri.verts[0]
	b := tri.verts[1]
	c := tri.verts[2]

	segs[0] = [2]encoding.Vertex{a, b}
	segs[1] = [2]encoding.Vertex{b, c}
	segs[2] = [2]encoding.Vertex{c, a}

	return segs
}
