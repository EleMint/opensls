package pkg

import (
	"bytes"
	"fmt"
	"math"
	"opensls/model/encoding"
	"os"
	"sort"
	"strings"
	"sync"
)

type Slice struct {
	Segments Segments
	Z        float32
	Step     int
}

func (s Slice) String() string {
	buf := strings.Builder{}

	buf.WriteString(fmt.Sprintf("\tStep: %v\tZ: %v\n", s.Step, s.Z))
	buf.WriteString("\t\tSegs: [\n")

	for _, seg := range s.Segments {
		buf.WriteString(fmt.Sprintf("\t\t\t- %s\n", seg))
	}

	buf.WriteString("\t\t]\n")

	return buf.String()
}

type Slices struct {
	mu     sync.Mutex
	Slices []Slice
}

func (ss *Slices) Insert(s Slice) {
	ss.mu.Lock()
	defer ss.mu.Unlock()

	i := sort.Search(len(ss.Slices), func(i int) bool {
		return s.Z < ss.Slices[i].Z
	})

	if i < len(ss.Slices) && ss.Slices[i].Z == s.Z {
		// slice is present in data[i]
	} else {
		// slice is not present data,
		// but i is the index where it would be inserted.
		ss.Slices = append(ss.Slices, Slice{})
		copy(ss.Slices[i+1:], ss.Slices[i:])
		ss.Slices[i] = s
	}
}

func (ss *Slices) MinX() (min float32) {
	ss.mu.Lock()
	defer ss.mu.Unlock()

	min = math.MaxFloat32

	for _, s := range ss.Slices {
		m := math.Min(float64(min), float64(s.Segments.MinX()))
		min = float32(m)
	}

	return
}

func (ss *Slices) MaxX() (max float32) {
	ss.mu.Lock()
	defer ss.mu.Unlock()

	max = math.SmallestNonzeroFloat32

	for _, s := range ss.Slices {
		m := math.Max(float64(max), float64(s.Segments.MaxX()))
		max = float32(m)
	}

	return
}

type Triangles []*Triangle

func (tris Triangles) Insert(tri *Triangle) Triangles {
	i := sort.Search(len(tris), func(i int) bool {
		return tri.MinZ() < tris[i].MinZ()
	})

	if i < len(tris) && tris[i] == tri {
		// tri is present in data[i]
	} else {
		// tri is not present in data,
		// but i is the index where it would be inserted.
		tris = append(tris, &Triangle{})
		copy(tris[i+1:], tris[i:])
		tris[i] = tri
	}

	return tris
}

func CalculateSlice(tris *Triangles, z float32, step int) (s Slice) {
	s.Segments = Segments{}
	s.Z = z
	s.Step = step

	caseXs := map[string]struct {
		pts  [2]encoding.Vertex
		norm *encoding.Normal
	}{}

	for _, tri := range *tris {
		c, pts := tri.Case(z)
		if IsActive(c) {
			seg := tri.Segment(c, pts, z)
			if seg == NilSegment {
				fmt.Fprintln(os.Stderr, "WARN: Active Triangle with NO Valid Segment")
				continue
			}
			s.Segments = s.Segments.Insert(seg)
		} else if c == CASE_X {
			segs := tri.CaseXPoints()
			for _, seg := range segs {
				id := bytes.Buffer{}
				id.Write(seg[0].Value())
				id.Write(seg[1].Value())

				_, found := caseXs[id.String()]
				if found {
					delete(caseXs, id.String())
				} else {
					caseXs[id.String()] = struct {
						pts  [2]encoding.Vertex
						norm *encoding.Normal
					}{
						pts:  seg,
						norm: tri.Norm(),
					}
				}
			}
		}
	}

	if len(caseXs) > 0 {
		for _, seg := range caseXs {
			s.Segments = append(s.Segments, Segment{
				Case:   CASE_X,
				Points: seg.pts,
				Normal: seg.norm,
			})
		}
	}

	return
}
