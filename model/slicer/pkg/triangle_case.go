package pkg

import "opensls/model/encoding"

type TriangleCase uint

const (
	CASE_UNKNOWN TriangleCase = iota
	// Case 1 denotes A being above the cutting plane while B and C are below.
	CASE_I
	// Case 2 denotes A being below the cutting plane while B and C are above.
	CASE_II
	// Case 3 denotes A being on the cutting plane while B and C are on opposite sides of the cutting plane.
	CASE_III
	// Case 4 denotes A being on the cutting plane while B and C are above the cutting plane.
	CASE_IV
	// Case 5 denotes A being on the cutting plane while B and C are below the cutting plane.
	CASE_V
	// Case 6 denotes A and B being on the cutting plane while C is above the cutting plane.
	CASE_VI
	// Case 7 denotes A and B being on the cutting plane while C is below the cutting plane.
	CASE_VII
	// Case 8 denotes A, B, and C being above the cutting plane.
	CASE_VIII
	// Case 9 denotes A, B, and C being below the cutting plane.
	CASE_IX
	// Case 10 denotes A, B, and C being on the cutting plane.
	CASE_X
)

func (tc TriangleCase) String() string {
	switch tc {
	case CASE_UNKNOWN:
		return "CASE_UNKNOWN"
	case CASE_I:
		return "CASE_I"
	case CASE_II:
		return "CASE_II"
	case CASE_III:
		return "CASE_III"
	case CASE_IV:
		return "CASE_IV"
	case CASE_V:
		return "CASE_V"
	case CASE_VI:
		return "CASE_VI"
	case CASE_VII:
		return "CASE_VII"
	case CASE_VIII:
		return "CASE_VIII"
	case CASE_IX:
		return "CASE_IX"
	case CASE_X:
		return "CASE_X"
	}

	return "CASE_UNKNOWN"
}

type Vert uint

const (
	NONE Vert = iota
	A
	B
	C
)

type CasePoints struct {
	Above []encoding.Vertex
	At    []encoding.Vertex
	Below []encoding.Vertex
}

var NilPoints = CasePoints{}

// isCaseI determines if A is above the cutting plane while B and C are below the cutting plane.
func (t Triangle) isCaseI(z float32) (bool, CasePoints) {
	a := t.verts[0].Z
	b := t.verts[1].Z
	c := t.verts[2].Z

	// find one above z
	above := NONE
	if a > z {
		above = A
	} else if b > z {
		above = B
	} else if c > z {
		above = C
	}

	// ensure the rest are below z
	switch above {
	case A:
		if !(b < z && c < z) {
			return false, NilPoints
		}
		return true, CasePoints{
			Above: []encoding.Vertex{t.verts[0]},
			Below: []encoding.Vertex{t.verts[1], t.verts[2]},
		}
	case B:
		if !(a < z && c < z) {
			return false, NilPoints
		}
		return true, CasePoints{
			Above: []encoding.Vertex{t.verts[1]},
			Below: []encoding.Vertex{t.verts[0], t.verts[2]},
		}
	case C:
		if !(a < z && b < z) {
			return false, NilPoints
		}
		return true, CasePoints{
			Above: []encoding.Vertex{t.verts[2]},
			Below: []encoding.Vertex{t.verts[0], t.verts[1]},
		}
	}

	return false, NilPoints
}

// isCaseII determines if A is below the cutting plane while B and C are above the cutting plane.
func (t Triangle) isCaseII(z float32) (bool, CasePoints) {
	a := t.verts[0].Z
	b := t.verts[1].Z
	c := t.verts[2].Z

	// find one below z
	below := NONE
	if a < z {
		below = A
	} else if b < z {
		below = B
	} else if c < z {
		below = C
	}

	// ensure the rest are above z
	switch below {
	case A:
		if !(b > z && c > z) {
			return false, NilPoints
		}
		return true, CasePoints{
			Below: []encoding.Vertex{t.verts[0]},
			Above: []encoding.Vertex{t.verts[1], t.verts[2]},
		}
	case B:
		if !(a > z && c > z) {
			return false, NilPoints
		}
		return true, CasePoints{
			Below: []encoding.Vertex{t.verts[1]},
			Above: []encoding.Vertex{t.verts[0], t.verts[2]},
		}
	case C:
		if !(a > z && b > z) {
			return false, NilPoints
		}
		return true, CasePoints{
			Below: []encoding.Vertex{t.verts[2]},
			Above: []encoding.Vertex{t.verts[0], t.verts[1]},
		}
	}

	return false, NilPoints
}

// isCaseIII determines if A is on the cutting plane while B and C are on opposite sides of the cutting plane.
func (t Triangle) isCaseIII(z float32) (bool, CasePoints) {
	a := t.verts[0].Z
	b := t.verts[1].Z
	c := t.verts[2].Z

	// find one at z
	at := NONE
	if a == z {
		at = A
	} else if b == z {
		at = B
	} else if c == z {
		at = C
	}

	// ensure the rest are opposing z
	switch at {
	case A:
		if b > z && c < z {
			return true, CasePoints{
				At:    []encoding.Vertex{t.verts[0]},
				Above: []encoding.Vertex{t.verts[1]},
				Below: []encoding.Vertex{t.verts[2]},
			}
		} else if b < z && c > z {
			return true, CasePoints{
				At:    []encoding.Vertex{t.verts[0]},
				Above: []encoding.Vertex{t.verts[2]},
				Below: []encoding.Vertex{t.verts[1]},
			}
		}
		return false, NilPoints
	case B:
		if a > z && c < z {
			return true, CasePoints{
				At:    []encoding.Vertex{t.verts[1]},
				Above: []encoding.Vertex{t.verts[0]},
				Below: []encoding.Vertex{t.verts[2]},
			}
		} else if a < z && c > z {
			return true, CasePoints{
				At:    []encoding.Vertex{t.verts[1]},
				Above: []encoding.Vertex{t.verts[2]},
				Below: []encoding.Vertex{t.verts[0]},
			}
		}
		return false, NilPoints
	case C:
		if a > z && b < z {
			return true, CasePoints{
				At:    []encoding.Vertex{t.verts[2]},
				Above: []encoding.Vertex{t.verts[0]},
				Below: []encoding.Vertex{t.verts[1]},
			}
		} else if a < z && b > z {
			return true, CasePoints{
				At:    []encoding.Vertex{t.verts[2]},
				Above: []encoding.Vertex{t.verts[1]},
				Below: []encoding.Vertex{t.verts[0]},
			}
		}
		return false, NilPoints
	}

	return false, NilPoints
}

// isCaseIV determines if A is on the cutting plane while B and C are above the cutting plane.
func (t Triangle) isCaseIV(z float32) (bool, CasePoints) {
	a := t.verts[0].Z
	b := t.verts[1].Z
	c := t.verts[2].Z

	// find one at z
	at := NONE
	if a == z {
		at = A
	} else if b == z {
		at = B
	} else if c == z {
		at = C
	}

	// ensure the rest are above
	switch at {
	case A:
		if !(b > z && c > z) {
			return false, NilPoints
		}
		return true, CasePoints{
			At:    []encoding.Vertex{t.verts[0]},
			Above: []encoding.Vertex{t.verts[1], t.verts[2]},
		}
	case B:
		if !(a > z && c > z) {
			return false, NilPoints
		}
		return true, CasePoints{
			At:    []encoding.Vertex{t.verts[1]},
			Above: []encoding.Vertex{t.verts[0], t.verts[2]},
		}
	case C:
		if !(a > z && b > z) {
			return false, NilPoints
		}
		return true, CasePoints{
			At:    []encoding.Vertex{t.verts[2]},
			Above: []encoding.Vertex{t.verts[0], t.verts[1]},
		}
	}

	return false, NilPoints
}

// isCaseV determines if A is on the cutting plane while B and C are below the cutting plane.
func (t Triangle) isCaseV(z float32) (bool, CasePoints) {
	a := t.verts[0].Z
	b := t.verts[1].Z
	c := t.verts[2].Z

	// find one at z
	at := NONE
	if a == z {
		at = A
	} else if b == z {
		at = B
	} else if c == z {
		at = C
	}

	// ensure the rest are above
	switch at {
	case A:
		if !(b < z && c < z) {
			return false, NilPoints
		}
		return true, CasePoints{
			At:    []encoding.Vertex{t.verts[0]},
			Below: []encoding.Vertex{t.verts[1], t.verts[2]},
		}
	case B:
		if !(a < z && c < z) {
			return false, NilPoints
		}
		return true, CasePoints{
			At:    []encoding.Vertex{t.verts[1]},
			Below: []encoding.Vertex{t.verts[0], t.verts[2]},
		}
	case C:
		if !(a < z && b < z) {
			return false, NilPoints
		}
		return true, CasePoints{
			At:    []encoding.Vertex{t.verts[2]},
			Below: []encoding.Vertex{t.verts[0], t.verts[1]},
		}
	}

	return false, NilPoints
}

// isCaseVI determines if A and B are on the cutting plane while C is above the cutting plane.
func (t Triangle) isCaseVI(z float32) (bool, CasePoints) {
	a := t.verts[0].Z
	b := t.verts[1].Z
	c := t.verts[2].Z

	// find one above z
	above := NONE
	if a > z {
		above = A
	} else if b > z {
		above = B
	} else if c > z {
		above = C
	}

	// ensure the rest are at z
	switch above {
	case A:
		if !(b == z && c == z) {
			return false, NilPoints
		}
		return true, CasePoints{
			Above: []encoding.Vertex{t.verts[0]},
			At:    []encoding.Vertex{t.verts[1], t.verts[2]},
		}
	case B:
		if !(a == z && c == z) {
			return false, NilPoints
		}
		return true, CasePoints{
			Above: []encoding.Vertex{t.verts[1]},
			At:    []encoding.Vertex{t.verts[0], t.verts[2]},
		}
	case C:
		if !(a == z && b == z) {
			return false, NilPoints
		}
		return true, CasePoints{
			Above: []encoding.Vertex{t.verts[2]},
			At:    []encoding.Vertex{t.verts[0], t.verts[1]},
		}
	}

	return false, NilPoints
}

// isCaseVII determines if A and B are on the cutting plane while C is below the cutting plane.
func (t Triangle) isCaseVII(z float32) (bool, CasePoints) {
	a := t.verts[0].Z
	b := t.verts[1].Z
	c := t.verts[2].Z

	// find one below z
	below := NONE
	if a < z {
		below = A
	} else if b < z {
		below = B
	} else if c < z {
		below = C
	}

	// ensure the rest are at z
	switch below {
	case A:
		if !(b == z && c == z) {
			return false, NilPoints
		}
		return true, CasePoints{
			Below: []encoding.Vertex{t.verts[0]},
			At:    []encoding.Vertex{t.verts[1], t.verts[2]},
		}
	case B:
		if !(a == z && c == z) {
			return false, NilPoints
		}
		return true, CasePoints{
			Below: []encoding.Vertex{t.verts[1]},
			At:    []encoding.Vertex{t.verts[0], t.verts[2]},
		}
	case C:
		if !(a == z && b == z) {
			return false, NilPoints
		}
		return true, CasePoints{
			Below: []encoding.Vertex{t.verts[2]},
			At:    []encoding.Vertex{t.verts[0], t.verts[1]},
		}
	}

	return false, NilPoints
}

// isCaseVIII determines if A, B, and C are above the cutting plane.
func (t Triangle) isCaseVIII(z float32) (bool, CasePoints) {
	a := t.verts[0].Z
	b := t.verts[1].Z
	c := t.verts[2].Z
	return a > z && b > z && c > z, NilPoints
}

// isCaseIX determines if A, B, and C are below the cutting plane.
func (t Triangle) isCaseIX(z float32) (bool, CasePoints) {
	a := t.verts[0].Z
	b := t.verts[1].Z
	c := t.verts[2].Z
	return a < z && b < z && c < z, NilPoints
}

// isCaseX determines if A, B, and C are on the cutting plane.
func (t Triangle) isCaseX(z float32) (bool, CasePoints) {
	a := t.verts[0].Z
	b := t.verts[1].Z
	c := t.verts[2].Z
	return a == z && b == z && c == z, NilPoints
}

func (t Triangle) Case(z float32) (TriangleCase, CasePoints) {
	if ok, pts := t.isCaseI(z); ok {
		return CASE_I, pts
	} else if ok, pts := t.isCaseII(z); ok {
		return CASE_II, pts
	} else if ok, pts := t.isCaseIII(z); ok {
		return CASE_III, pts
	} else if ok, pts := t.isCaseIV(z); ok {
		return CASE_IV, pts
	} else if ok, pts := t.isCaseV(z); ok {
		return CASE_V, pts
	} else if ok, pts := t.isCaseVI(z); ok {
		return CASE_VI, pts
	} else if ok, pts := t.isCaseVII(z); ok {
		return CASE_VII, pts
	} else if ok, pts := t.isCaseVIII(z); ok {
		return CASE_VIII, pts
	} else if ok, pts := t.isCaseIX(z); ok {
		return CASE_IX, pts
	} else if ok, pts := t.isCaseX(z); ok {
		return CASE_X, pts
	}

	return CASE_UNKNOWN, NilPoints
}
