package main

import (
	"fmt"
	"math"
	"os"
	"sync"

	"opensls/model/encoding/stl"
	"opensls/model/slicer/pkg"
)

func fatalf(format string, args ...any) {
	fmt.Fprintf(os.Stderr, format, args...)
	fmt.Fprintln(os.Stderr, "")
	os.Exit(1)
}

func main() {
	if len(os.Args) < 2 {
		fatalf("no input file supplied")
	}

	file, err := os.Open(os.Args[1])
	if err != nil {
		fatalf("failed to open input file: %s", err)
	}
	defer file.Close()

	model, err := stl.ParseModel(file)
	if err != nil {
		fatalf("failed to parse input file as .StL: %s", err)
	}

	tris := pkg.Triangles{}

	for si, s := range model.Solids {
		fmt.Printf("solid %d: facet count %d\n", si, len(s))
		for _, f := range s {
			tris = tris.Insert(pkg.NewTriangle(f.N, f.A, f.B, f.C))
		}
	}

	fmt.Printf("done reading triangles (%d)\n", len(tris))

	minZ := tris[0].MinZ()
	maxZ := tris[len(tris)-1].MaxZ()
	stepSize := float32(0.05)
	stepCount := int((maxZ - minZ) / stepSize)

	fmt.Println("step count: ", stepCount)

	wg := sync.WaitGroup{}
	ls := pkg.Layers{}

	for step := 0; step <= stepCount; step++ {
		z := minZ + (stepSize * float32(step))
		z = float32(math.Round(float64(z)*1000) / 1000)

		wg.Add(1)
		go func(wg *sync.WaitGroup, z float32, step int) {
			defer wg.Done()

			slice := pkg.CalculateSlice(&tris, z, step)
			layer := pkg.CalculateLayer(slice)
			ls.Insert(layer)
		}(&wg, z, step)
	}

	wg.Wait()

	fmt.Println("layer count: ", len(ls.Layers))

	// for li, layer := range ls.Layers {
	// 	fmt.Printf("%d %v\n", li, layer.Z)
	// }
}
