package encoding

import (
	"bytes"
	"fmt"
	"strings"
)

type Normal struct {
	I, J, K float32
}

func (n Normal) Value() []byte {
	buf := bytes.Buffer{}

	buf.WriteString(fmt.Sprintf(
		"%x%x%x", n.I, n.J, n.K,
	))

	return buf.Bytes()
}

func (n Normal) String() string {
	buf := strings.Builder{}

	buf.WriteString(fmt.Sprintf(
		"{Normal I: %v, J: %v, K: %v}", n.I, n.J, n.K,
	))

	return buf.String()
}
