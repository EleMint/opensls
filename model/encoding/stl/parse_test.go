package stl

import (
	"bytes"
	"fmt"
	"io"
	"opensls/model/encoding"
	"os"
	"path/filepath"
	"testing"
)

var model encoding.Model

func benchParse(b *testing.B, r io.Reader) {
	model, err := ParseModel(r)
	if err != nil {
		b.Fatalf("failed to parse model: %s", err)
	}

	for si, s := range model.Solids {
		fmt.Printf("solid %d: facet count %d\n", si, len(s))
	}
}

func BenchmarkParseModel(b *testing.B) {
	name := "iso-sphere 8 subdivision"
	inputFile := "iso-sphere-08.stl"

	b.Run(name, func(b *testing.B) {
		p := filepath.Join("testdata", inputFile)
		buf, err := os.ReadFile(p)
		if err != nil {
			b.Fatalf("failed to read bench input file: %s", err)
		}

		input := bytes.NewBuffer(buf)

		b.Logf("%s size: %d\n", inputFile, input.Len())

		curr := b.Elapsed()

		b.Logf("read input file (%v)\n", curr)

		_, err = ParseModel(input)
		if err != nil {
			b.Fatalf("failed to read model: %s", err)
		}

		b.Logf("parsed model (%v)\n", b.Elapsed()-curr)
	})
}
