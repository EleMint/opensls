package stl

import (
	"io"

	"opensls/model/encoding"
	"opensls/model/encoding/stl/internal"
)

func ParseNormal(r io.Reader) (n encoding.Normal, err error) {
	cur := internal.NewCursor(r)
	n, err = internal.ReadNormal(cur)
	return
}

func ParseVertex(r io.Reader) (v encoding.Vertex, err error) {
	cur := internal.NewCursor(r)
	v, err = internal.ReadVertex(cur)
	return
}

func ParseFacet(r io.Reader) (f encoding.Facet, err error) {
	cur := internal.NewCursor(r)
	f, err = internal.ReadFacet(cur)
	return
}

func ParseSolid(r io.Reader) (s encoding.Solid, err error) {
	cur := internal.NewCursor(r)
	s, err = internal.ReadSolid(cur)
	return
}

func ParseModel(r io.Reader) (m encoding.Model, err error) {
	cur := internal.NewCursor(r)
	m, err = internal.ReadModel(cur)
	return
}
