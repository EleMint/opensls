package internal

import (
	"io"

	"opensls/model/encoding"

	"github.com/pkg/errors"
)

func ReadNormal(cur *Cursor) (normal encoding.Normal, err error) {
	normal.I, err = cur.ReadFloat32()
	if err != nil {
		err = errors.Wrap(err, "could not read normal.I (float32)")
		return
	}

	normal.J, err = cur.ReadFloat32()
	if err != nil {
		err = errors.Wrap(err, "could not read normal.J (float32)")
		return
	}

	normal.K, err = cur.ReadFloat32()
	if err != nil {
		err = errors.Wrap(err, "could not read normal.K (float32)")
		return
	}

	return
}

func ReadVertex(cur *Cursor) (vertex encoding.Vertex, err error) {
	vertex.X, err = cur.ReadFloat32()
	if err != nil {
		err = errors.Wrap(err, "could not read vertex.X (float32)")
		return
	}

	vertex.Y, err = cur.ReadFloat32()
	if err != nil {
		err = errors.Wrap(err, "could not read vertex.Y (float32)")
		return
	}

	vertex.Z, err = cur.ReadFloat32()
	if err != nil {
		err = errors.Wrap(err, "could not read vertex.Z (float32)")
		return
	}

	return
}

func ReadFacet(cur *Cursor) (facet encoding.Facet, err error) {
	facet.N, err = ReadNormal(cur)
	if err != nil {
		err = errors.Wrap(err, "could not read facet.N (normal)")
		return
	}

	facet.A, err = ReadVertex(cur)
	if err != nil {
		err = errors.Wrap(err, "could not read facet.A (vertex)")
		return
	}

	facet.B, err = ReadVertex(cur)
	if err != nil {
		err = errors.Wrap(err, "could not read facet.B (vertex)")
		return
	}

	facet.C, err = ReadVertex(cur)
	if err != nil {
		err = errors.Wrap(err, "could not read facet.C (vertex)")
		return
	}

	facet.Attrs, err = cur.ReadUint16()
	if err != nil {
		err = errors.Wrap(err, "could not read facet.Attrs (uint16)")
		return
	}

	return
}

func ReadSolid(cur *Cursor) (solid encoding.Solid, err error) {
	var count uint32

	count, err = cur.ReadUint32()
	if err != nil {
		err = errors.Wrap(err, "could not read solid facet count (uint32)")
		return
	}

	var i uint32
	var facet encoding.Facet

	for ; i < count; i++ {
		facet, err = ReadFacet(cur)
		if err != nil {
			err = errors.Wrap(err, "could not read solid facet")
			return
		}

		solid = append(solid, facet)
	}

	return
}

func trim(xs []byte) []byte {
	if len(xs) == 0 {
		return xs
	}

	if xs[len(xs)-1] != 0 {
		return xs
	}

	return trim(xs[:len(xs)-1])
}

const HEADER_LENGTH = 80

func ReadHeader(c *Cursor) (string, error) {
	buf, err := c.Read(HEADER_LENGTH)
	if err != nil {
		return "", err
	}

	buf = trim(buf)

	return string(buf), nil
}

func ReadModel(cur *Cursor) (model encoding.Model, err error) {
	model.Header, err = ReadHeader(cur)
	if err != nil {
		err = errors.Wrapf(err, "could not read model header ([%d]byte)", HEADER_LENGTH)
		return
	}

	var solid encoding.Solid

	for {
		solid, err = ReadSolid(cur)
		if err != nil {
			if errors.Is(err, io.EOF) {
				err = nil
				break
			}

			err = errors.Wrap(err, "could not read model solid(s)")
			return
		}

		model.Solids = append(model.Solids, solid)
	}

	return
}
