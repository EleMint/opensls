package internal

import (
	"bytes"
	"io"
	"math"
	"testing"

	"github.com/pkg/errors"
)

func TestRead(t *testing.T) {
	var tests = []struct {
		name string
		n    uint
		want []byte
		err  error
	}{
		{"should allow 0 bytes read", 0, []byte(""), nil},
		{"should allow 1 byte read", 1, []byte("a"), nil},
		{"should allow read whole input", 26, []byte("abcdefghijklmnopqrstuvwxyz"), nil},
		{"should return EOF if read buffer len != input size", 100, []byte(""), io.EOF},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			input := bytes.NewBufferString("abcdefghijklmnopqrstuvwxyz")

			cur := NewCursor(input)

			actual, err := cur.Read(tt.n)
			if err != nil && !errors.Is(err, tt.err) {
				t.Fatalf("expected error %s; got %s", tt.err, err)
			}

			if string(actual) != string(tt.want) {
				t.Fatalf("expected value %s; got %s", tt.want, actual)
			}
		})
	}
}

func TestReadUint16(t *testing.T) {
	var tests = []struct {
		name  string
		input []byte
		want  uint16
	}{
		{"should read a zero value", []byte{0, 0}, 0},
		{"should read a uint16", []byte{127, 127}, 32639},
		{"should read a max uint16", []byte{255, 255}, math.MaxUint16},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			buf := bytes.NewBuffer(tt.input)

			cur := NewCursor(buf)

			actual, err := cur.ReadUint16()
			if err != nil {
				t.Fatalf("unexpected error %s", err)
			}

			if actual != tt.want {
				t.Fatalf("expected value %d; got %d", tt.want, actual)
			}
		})
	}
}

func TestReadUint32(t *testing.T) {
	var tests = []struct {
		name  string
		input []byte
		want  uint32
	}{
		{"should read a zero value", []byte{0, 0, 0, 0}, 0},
		{"should read a uint32", []byte{127, 127, 127, 127}, 2139062143},
		{"should read a max uint32", []byte{255, 255, 255, 255}, math.MaxUint32},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			buf := bytes.NewBuffer(tt.input)

			cur := NewCursor(buf)

			actual, err := cur.ReadUint32()
			if err != nil {
				t.Fatalf("unexpected error %s", err)
			}

			if actual != tt.want {
				t.Fatalf("expected value %d; got %d", tt.want, actual)
			}
		})
	}
}

func TestReadFloat32(t *testing.T) {
	var tests = []struct {
		name  string
		input []byte
		want  float32
	}{
		{"should read a zero value", []byte{0, 0, 0, 0}, 0},
		{"should read a float32", []byte{76, 220, 33, 192}, -2.5290709},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			buf := bytes.NewBuffer(tt.input)

			cur := NewCursor(buf)

			actual, err := cur.ReadFloat32()
			if err != nil {
				t.Fatalf("unexpected error %s", err)
			}

			if actual != tt.want {
				t.Fatalf("expected value %v; got %v", tt.want, actual)
			}
		})
	}
}
