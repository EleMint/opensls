package internal

import (
	"bytes"
	"os"
	"path/filepath"
	"testing"

	"opensls/model/encoding"
)

func TestReadNormal(t *testing.T) {
	var tests = []struct {
		name  string
		input []byte
		want  encoding.Normal
	}{
		{
			"should read 0 normal",
			make([]byte, 100, 100),
			encoding.Normal{},
		},
		{
			"should read normal",
			[]byte{
				76, 220, 33, 192, // I
				76, 220, 33, 192, // J
				76, 220, 33, 192, // K
			},
			encoding.Normal{I: -2.5290709, J: -2.5290709, K: -2.5290709},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			input := bytes.NewBuffer(tt.input)

			cur := NewCursor(input)

			actual, err := ReadNormal(cur)
			if err != nil {
				t.Fatalf("expected error %s", err)
			}

			if string(actual.Value()) != string(tt.want.Value()) {
				t.Fatalf("expected value %s; got %s", tt.want, actual)
			}
		})
	}
}

func TestReadVertex(t *testing.T) {
	var tests = []struct {
		name  string
		input []byte
		want  encoding.Vertex
	}{
		{
			"should read 0 vertex",
			make([]byte, 100, 100),
			encoding.Vertex{},
		},
		{
			"should read vertex",
			[]byte{
				76, 220, 33, 192, // X
				76, 220, 33, 192, // Y
				76, 220, 33, 192, // Z
			},
			encoding.Vertex{X: -2.5290709, Y: -2.5290709, Z: -2.5290709},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			input := bytes.NewBuffer(tt.input)

			cur := NewCursor(input)

			actual, err := ReadVertex(cur)
			if err != nil {
				t.Fatalf("expected error %s", err)
			}

			if string(actual.Value()) != string(tt.want.Value()) {
				t.Fatalf("expected value %s; got %s", tt.want, actual)
			}
		})
	}
}

func TestReadFacet(t *testing.T) {
	var tests = []struct {
		name  string
		input []byte
		want  encoding.Facet
	}{
		{
			"should read 0 facet",
			make([]byte, 100, 100),
			encoding.Facet{},
		},
		{
			"should read facet",
			[]byte{
				76, 220, 33, 192, // nI
				76, 220, 33, 192, // nJ
				76, 220, 33, 192, // nK
				76, 220, 33, 192, // 1A
				76, 220, 33, 192, // 1J
				76, 220, 33, 192, // 1K
				76, 220, 33, 192, // 2I
				76, 220, 33, 192, // 2J
				76, 220, 33, 192, // 2K
				76, 220, 33, 192, // 3I
				76, 220, 33, 192, // 3J
				76, 220, 33, 192, // 3K
				0, 0, // attrs
			},
			encoding.Facet{
				N:     encoding.Normal{I: -2.5290709, J: -2.5290709, K: -2.5290709},
				A:     encoding.Vertex{X: -2.5290709, Y: -2.5290709, Z: -2.5290709},
				B:     encoding.Vertex{X: -2.5290709, Y: -2.5290709, Z: -2.5290709},
				C:     encoding.Vertex{X: -2.5290709, Y: -2.5290709, Z: -2.5290709},
				Attrs: 0,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			input := bytes.NewBuffer(tt.input)

			cur := NewCursor(input)

			actual, err := ReadFacet(cur)
			if err != nil {
				t.Fatalf("expected error %s", err)
			}

			if string(actual.Value()) != string(tt.want.Value()) {
				t.Fatalf("expected value %s; got %s", tt.want, actual)
			}
		})
	}
}

func TestReadSolid(t *testing.T) {
	var tests = []struct {
		name  string
		input []byte
		want  encoding.Solid
	}{
		{
			"should read 0 solid",
			make([]byte, 100, 100),
			encoding.Solid{},
		},
		{
			"should read solid",
			[]byte{
				1, 0, 0, 0,
				76, 220, 33, 192, // nI
				76, 220, 33, 192, // nJ
				76, 220, 33, 192, // nK
				76, 220, 33, 192, // 1A
				76, 220, 33, 192, // 1J
				76, 220, 33, 192, // 1K
				76, 220, 33, 192, // 2I
				76, 220, 33, 192, // 2J
				76, 220, 33, 192, // 2K
				76, 220, 33, 192, // 3I
				76, 220, 33, 192, // 3J
				76, 220, 33, 192, // 3K
				0, 0, // attrs
			},
			encoding.Solid{
				encoding.Facet{
					N:     encoding.Normal{I: -2.5290709, J: -2.5290709, K: -2.5290709},
					A:     encoding.Vertex{X: -2.5290709, Y: -2.5290709, Z: -2.5290709},
					B:     encoding.Vertex{X: -2.5290709, Y: -2.5290709, Z: -2.5290709},
					C:     encoding.Vertex{X: -2.5290709, Y: -2.5290709, Z: -2.5290709},
					Attrs: 0,
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			input := bytes.NewBuffer(tt.input)

			cur := NewCursor(input)

			actual, err := ReadSolid(cur)
			if err != nil {
				t.Fatalf("expected error %s", err)
			}

			if string(actual.Value()) != string(tt.want.Value()) {
				t.Fatalf("expected value %s; got %s", tt.want, actual)
			}
		})
	}
}

func TestReadHeader(t *testing.T) {
	var tests = []struct {
		name  string
		input []byte
		want  string
	}{
		{
			"should read empty header",
			make([]byte, 100, 100),
			"",
		},
		{
			"should read header",
			[]byte{
				't', 'e', 's', 't', 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0,
			},
			"test",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			input := bytes.NewBuffer(tt.input)

			cur := NewCursor(input)

			actual, err := ReadHeader(cur)
			if err != nil {
				t.Fatalf("expected error %s", err)
			}

			if actual != tt.want {
				t.Fatalf("expected value %s; got %s", tt.want, actual)
			}
		})
	}
}

func TestReadModel(t *testing.T) {
	var tests = []struct {
		name      string
		inputFile string
		want      encoding.Model
	}{
		{
			"should read zero file",
			"empty.binary.stl",
			encoding.Model{},
		},
		{
			"should read file",
			"cube.binary.stl",
			encoding.Model{
				Header: "Exported from Blender-3.5.0",
				Solids: []encoding.Solid{
					[]encoding.Facet{
						{
							N:     encoding.Normal{I: 0.0, J: 0.0, K: 1.0},
							A:     encoding.Vertex{X: -1.0, Y: -1.0, Z: 1.0},
							B:     encoding.Vertex{X: 1.0, Y: -1.0, Z: 1.0},
							C:     encoding.Vertex{X: 1.0, Y: 1.0, Z: 1.0},
							Attrs: 0,
						},
						{
							N:     encoding.Normal{I: 0.0, J: 0.0, K: 1.0},
							A:     encoding.Vertex{X: -1.0, Y: -1.0, Z: 1.0},
							B:     encoding.Vertex{X: 1.0, Y: 1.0, Z: 1.0},
							C:     encoding.Vertex{X: -1.0, Y: 1.0, Z: 1.0},
							Attrs: 0,
						},
						{
							N:     encoding.Normal{I: 0.0, J: 1.0, K: 0.0},
							A:     encoding.Vertex{X: -1.0, Y: 1.0, Z: -1.0},
							B:     encoding.Vertex{X: -1.0, Y: 1.0, Z: 1.0},
							C:     encoding.Vertex{X: 1.0, Y: 1.0, Z: 1.0},
							Attrs: 0,
						},
						{
							N:     encoding.Normal{I: 0.0, J: 1.0, K: 0.0},
							A:     encoding.Vertex{X: -1.0, Y: 1.0, Z: -1.0},
							B:     encoding.Vertex{X: 1.0, Y: 1.0, Z: 1.0},
							C:     encoding.Vertex{X: 1.0, Y: 1.0, Z: -1.0},
							Attrs: 0,
						},
						{
							N:     encoding.Normal{I: 1.0, J: 0.0, K: 0.0},
							A:     encoding.Vertex{X: 1.0, Y: 1.0, Z: -1.0},
							B:     encoding.Vertex{X: 1.0, Y: 1.0, Z: 1.0},
							C:     encoding.Vertex{X: 1.0, Y: -1.0, Z: 1.0},
							Attrs: 0,
						},
						{
							N:     encoding.Normal{I: 1.0, J: 0.0, K: 0.0},
							A:     encoding.Vertex{X: 1.0, Y: 1.0, Z: -1.0},
							B:     encoding.Vertex{X: 1.0, Y: -1.0, Z: 1.0},
							C:     encoding.Vertex{X: 1.0, Y: -1.0, Z: -1.0},
							Attrs: 0,
						},
						{
							N:     encoding.Normal{I: 0.0, J: 0.0, K: -1.0},
							A:     encoding.Vertex{X: 1.0, Y: -1.0, Z: -1.0},
							B:     encoding.Vertex{X: -1.0, Y: -1.0, Z: -1.0},
							C:     encoding.Vertex{X: -1.0, Y: 1.0, Z: -1.0},
							Attrs: 0,
						},
						{
							N:     encoding.Normal{I: 0.0, J: 0.0, K: -1.0},
							A:     encoding.Vertex{X: 1.0, Y: -1.0, Z: -1.0},
							B:     encoding.Vertex{X: -1.0, Y: 1.0, Z: -1.0},
							C:     encoding.Vertex{X: 1.0, Y: 1.0, Z: -1.0},
							Attrs: 0,
						},
						{
							N:     encoding.Normal{I: -1.0, J: 0.0, K: 0.0},
							A:     encoding.Vertex{X: -1.0, Y: -1.0, Z: -1.0},
							B:     encoding.Vertex{X: -1.0, Y: -1.0, Z: 1.0},
							C:     encoding.Vertex{X: -1.0, Y: 1.0, Z: 1.0},
							Attrs: 0,
						},
						{
							N:     encoding.Normal{I: -1.0, J: 0.0, K: 0.0},
							A:     encoding.Vertex{X: -1.0, Y: -1.0, Z: -1.0},
							B:     encoding.Vertex{X: -1.0, Y: 1.0, Z: 1.0},
							C:     encoding.Vertex{X: -1.0, Y: 1.0, Z: -1.0},
							Attrs: 0,
						},
						{
							N:     encoding.Normal{I: 0.0, J: -1.0, K: 0.0},
							A:     encoding.Vertex{X: 1.0, Y: -1.0, Z: -1.0},
							B:     encoding.Vertex{X: 1.0, Y: -1.0, Z: 1.0},
							C:     encoding.Vertex{X: -1.0, Y: -1.0, Z: 1.0},
							Attrs: 0,
						},
						{
							N:     encoding.Normal{I: 0.0, J: -1.0, K: 0.0},
							A:     encoding.Vertex{X: 1.0, Y: -1.0, Z: -1.0},
							B:     encoding.Vertex{X: -1.0, Y: -1.0, Z: 1.0},
							C:     encoding.Vertex{X: -1.0, Y: -1.0, Z: -1.0},
							Attrs: 0,
						},
					},
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := filepath.Join("testdata", tt.inputFile)
			buf, err := os.ReadFile(p)
			if err != nil {
				t.Fatalf("failed to read input file: %s", err)
			}

			input := bytes.NewBuffer(buf)

			cur := NewCursor(input)

			actual, err := ReadModel(cur)
			if err != nil {
				t.Fatalf("expected error %s", err)
			}

			if actual.Header != tt.want.Header {
				t.Fatalf("expected header %s; got %s", tt.want.Header, actual.Header)
			}

			for i, s := range tt.want.Solids {
				gotS := actual.Solids[i]

				for j, f := range s {
					gotF := gotS[j]

					if string(f.Value()) != string(gotF.Value()) {
						t.Fatalf(
							"expected solid (%d) facet (%d) %s\ngot solid facet %s\n",
							i,
							j,
							f,
							gotF,
						)
					}
				}
			}
		})
	}
}
