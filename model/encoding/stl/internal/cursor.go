package internal

import (
	"encoding/binary"
	"io"
	"math"
)

const (
	size_of_uint16  = 2
	size_of_uint32  = 4
	size_of_float32 = 4
)

type Cursor struct {
	r io.Reader
}

func NewCursor(r io.Reader) *Cursor {
	return &Cursor{
		r: r,
	}
}

func (c *Cursor) Read(size uint) (buf []byte, err error) {
	lr := io.LimitReader(c.r, int64(size))

	buf, err = io.ReadAll(lr)
	if err != nil {
		return nil, err
	}

	if uint(len(buf)) != size {
		return nil, io.EOF
	}

	return
}

func (c *Cursor) ReadUint16() (value uint16, err error) {
	var buf []byte

	buf, err = c.Read(size_of_uint16)
	if err != nil {
		return
	}

	value = binary.LittleEndian.Uint16(buf)

	return value, nil
}

func (c *Cursor) ReadUint32() (value uint32, err error) {
	var buf []byte

	buf, err = c.Read(size_of_uint32)
	if err != nil {
		return
	}

	value = binary.LittleEndian.Uint32(buf)

	return value, nil
}

func (c *Cursor) ReadFloat32() (value float32, err error) {
	var buf uint32

	buf, err = c.ReadUint32()
	if err != nil {
		return
	}

	value = math.Float32frombits(buf)

	return
}
