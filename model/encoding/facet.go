package encoding

import (
	"bytes"
	"fmt"
	"strings"
)

type Facet struct {
	N Normal

	A Vertex
	B Vertex
	C Vertex

	Attrs uint16
}

func (f Facet) Value() []byte {
	buf := bytes.Buffer{}

	buf.Write(f.N.Value())
	buf.Write(f.A.Value())
	buf.Write(f.B.Value())
	buf.Write(f.C.Value())
	buf.WriteString(fmt.Sprintf(
		"%x", f.Attrs,
	))

	return buf.Bytes()
}

func (f Facet) String() string {
	buf := strings.Builder{}

	buf.WriteString(fmt.Sprintf(
		"{Facet %s, %s, %s, %s, Attrs: %d}",
		f.N, f.A, f.B, f.C, f.Attrs,
	))

	return buf.String()
}
