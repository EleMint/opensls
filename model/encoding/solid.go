package encoding

import (
	"bytes"
	"fmt"
	"strings"
)

type Solid []Facet

func (s Solid) Value() []byte {
	buf := bytes.Buffer{}

	for _, f := range s {
		buf.Write(f.Value())
	}

	return buf.Bytes()
}

func (s Solid) String() string {
	buf := strings.Builder{}

	buf.WriteString("\n")

	for _, f := range s {
		buf.WriteString(fmt.Sprintf("\t\t\t%s,\n", f))
	}

	return buf.String()
}
