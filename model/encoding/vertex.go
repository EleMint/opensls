package encoding

import (
	"bytes"
	"fmt"
	"strings"
)

type Vertex struct {
	X, Y, Z float32
}

func (v Vertex) Value() []byte {
	buf := bytes.Buffer{}

	buf.WriteString(fmt.Sprintf(
		"%x%x%x", v.X, v.Y, v.Z,
	))

	return buf.Bytes()
}

func (v Vertex) String() string {
	buf := strings.Builder{}

	buf.WriteString(fmt.Sprintf(
		"{Vertex X: %v, Y: %v, Z: %v}", v.X, v.Y, v.Z,
	))

	return buf.String()
}
