package encoding

import (
	"bytes"
	"fmt"
	"strings"
)

type Model struct {
	Header string
	Solids []Solid
}

func (m Model) Value() []byte {
	buf := bytes.Buffer{}

	buf.WriteString(m.Header)

	for _, s := range m.Solids {
		buf.Write(s.Value())
	}

	return buf.Bytes()
}

func (m Model) String() string {
	buf := strings.Builder{}

	buf.WriteString("{Model\n")

	buf.WriteString(fmt.Sprintf(
		"\tHeader: '%s',\n", m.Header,
	))

	buf.WriteString("\tSolids: [\n")
	for i, s := range m.Solids {
		buf.WriteString(fmt.Sprintf("\t\t%d. %s\n", i, s))
	}
	buf.WriteString("\t]\n")

	buf.WriteString("}")

	return buf.String()
}
